<?php

namespace App\Presenters;

use Nette;


class HomepagePresenter extends Nette\Application\UI\Presenter
{


    protected function createComponentTranslatorForm()
    {
        $form = (new \TranslatorFormFactory())->create();

        $form->onSuccess[] = [$this, 'onTranslatorFormSuccess'];

        return $form;
    }

    public function onTranslatorFormSuccess(\Nette\Application\UI\Form $form, \stdClass $values)
    {

        $translator = new \Phebix\PigLatinTranslator();

        if ($values->useHyphen) {
            $translator->setUseHyphen(true);
        }

        if (!empty($values->vowel)) {
            $translator->setVowelSuffix($values->vowel);
        }


        $pigLatinText = $translator->translate($values->text);

        $this->template->pigLatinText = $pigLatinText;
    }


}
