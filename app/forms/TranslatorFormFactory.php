<?php

use Nette\Application\UI\Form;

class TranslatorFormFactory
{

    public function create()
    {
        $form = new Form;

        $form->addTextArea('text', 'Text:')
            ->setRequired('Please enter text for translation');

        $form->addSelect('useHyphen', 'Use hyphen:', [0 => 'no', 1 => 'yes']);

        $form->addSelect('vowel', 'Vowel suffix:', [
            \Phebix\PigLatinTranslator::VOWEL_SUFFIX_AY  => \Phebix\PigLatinTranslator::VOWEL_SUFFIX_AY,
            \Phebix\PigLatinTranslator::VOWEL_SUFFIX_YAY => \Phebix\PigLatinTranslator::VOWEL_SUFFIX_YAY,
            \Phebix\PigLatinTranslator::VOWEL_SUFFIX_WAY => \Phebix\PigLatinTranslator::VOWEL_SUFFIX_WAY,
            \Phebix\PigLatinTranslator::VOWEL_SUFFIX_HAY => \Phebix\PigLatinTranslator::VOWEL_SUFFIX_HAY,
        ]);

        $form->addSubmit('btSend', 'Translate');

        return $form;
    }

}