Pig Latin Example
=================

This is a simple example of using the [Pig Latin Tranlator](https://gitlab.com/_phebix/public/php/pig-latin)



Requirements
------------

PHP 7.0 or higher.



Instalation
-----------

Clone this repo
```
git clone https://gitlab.com/_phebix/public/php/pig-latin-example.git
```

Create log directory and set permissions
```
cd pig-latin-example && mkdir log && chmod -R a+rw temp log
```

